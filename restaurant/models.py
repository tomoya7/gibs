from django.db import models
from django.urls import reverse

# Create your models here.

class Restaurant(models.Model):
    "店舗テーブル"
    #storeからnameへ,TextFieldからCharField
    id = models.CharField(verbose_name='店舗ID',max_length=10,primary_key=True)
    name = models.CharField(verbose_name='店舗名',max_length=20)
    category = models.CharField(verbose_name='カテゴリ',max_length=10)
    address = models.CharField(verbose_name='メールアドレス',max_length=128)
    latitude = models.FloatField(verbose_name='緯度')
    longitude =  models.FloatField(verbose_name='経度')
    url = models.CharField(verbose_name='URL',max_length=500)
    image_url = models.CharField(verbose_name='画像url',max_length=500)
    store_update_date  = models.DateTimeField(verbose_name='更新日時',auto_now=True)


    class Meta:
        verbose_name_plural = 'restaurant'

    def __str__(self):
        return self.name
