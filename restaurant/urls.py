from django.urls import path
from . import views

app_name = 'restaurant'
urlpatterns = [
    path('', views.ShareView.as_view(), name="share"),
path('share-detail/<int:pk>/', views.ShareDetailView.as_view(), name="share_detail"),
]