from django.views import generic

from .models import restaurant

# Create your views here.

class ShareView(generic.ListView):
    model = restaurant
    template_name = "share.html"

class ShareDetailView(generic.DetailView):
    model = restaurant
    template_name = 'share_detail.html'